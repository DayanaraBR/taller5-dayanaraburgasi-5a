const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
const nombre = document.getElementById('name');
const correo = document.getElementById('email');
const contraseña = document.getElementById('pasw');
const apellido = document.getElementById('apellido');
const telefono = document.getElementById('telf');
const c_contraseña = document.getElementById('c-pasw');
const adver = document.getElementById('warning');

 const campos = {
     nombre: false,
     correo: false,
     contraseña: false,
     apellido: false,
     telefono: false,
     c_contraseña: false


 }

const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /([a-z\d]+[@]+[a-z]+\.[a-z]{2,})/,
    apellido: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,
    numeros : /([0-9])/,
    text : /([a-zA-Z])/,
    caracteres : /[^a-zA-Z\d\s]/,
    correo : /([a-z\d]+[@]+[a-z]+\.[a-z]{2,})/,
    espacios : /\s/g,
	telefono: /^\d{0,10}$/ // 7 a 14 numeros.
}

const validarFormulario = (e) => {
    var ok = true;
    switch(e.target.name){
        case"name":
            nombre.value = e.target.value
            .replace(expresiones.espacios, '')
            .replace(expresiones.numeros, '');
            
            if(expresiones.nombre.test(e.target.value)){
                nombre.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                ok=false;
                
            } else{
                
                nombre.style.border = '2px solid #ce1212';
                adver.innerHTML= "El nombre contiene simbolos.Escriba nuevamnete su nombre";  
                
            }

        break;
        case"mail":
            
            if(expresiones.correo.test(e.target.value)){
                correo.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                ok=false;
                
            } else{
                
                correo.style.border = '2px solid #ce1212';
                adver.innerHTML= "El correo esta incompleto o esta mal escrito.";
                
            } 
             
        break;
        case "pasw":
            contraseña.value = e.target.value
            .replace(expresiones.espacios, '');
            
            if(e.target.value.length < 8 ){
                contraseña.style.border = '2px solid #ce1212';
                adver.innerHTML= "Ingrese una contraseña mas de 8 digitos";
                ok=false;
            } else{
                contraseña.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                
            }
        break;
        case"last":
            apellido.value = e.target.value
            .replace(expresiones.numeros, '');
            
            if(expresiones.apellido.test(e.target.value)){
                apellido.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                ok=false;
            } else{
                
                apellido.style.border = '2px solid #ce1212';
                adver.innerHTML= "El apellido contiene simbolos.Escriba nuevamente su nombre";  
                
            }
        break;
        case"telf":
            telefono.value = e.target.value
            .replace(expresiones.text, '');
            
            if(expresiones.telefono.test(e.target.value)){
                telefono.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                ok=false;
            } else{
                
                telefono.style.border = '2px solid #ce1212';
                adver.innerHTML= "Ingrese numeros no letras";  
                
            }
        break;
        case"c-pasw":
            if(e.target.value != contraseña.value){
                c_contraseña.style.border = '2px solid #ce1212';
                adver.innerHTML= "La contraseñas no coinciden"; 
                ok=false;
                
            } else{
                c_contraseña.style.border = '2px solid #016BE4';
                adver.innerHTML = "";
                
                
            }
        break;
    }
}

inputs.forEach((input) =>{
    input.addEventListener('keyup',validarFormulario );
    input.addEventListener('blur',validarFormulario );
});


formulario.addEventListener("submit", e=>{
    e.preventDefault();
    console.log('gg')
    let warnings ="";
    let entrar  = false;

    inputs.forEach(e=>{
        if(e.value == ''){
            alert("Todos los campos son requeridos") ;
            entrar = false;
            return;
        }else{
            entrar = true;
        }
    });

    if(entrar == false){
        adver.innerHTML = warnings;
    }else{
        alert("Enviado");
    }

    formulario.reset();
    
    
})